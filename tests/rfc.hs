module Main where
import Network.URI.Template as P
import qualified Test.Framework as TF
import qualified Test.Framework.Providers.HUnit as TFH
import Test.HUnit

mkEnv :: [(String, String)] -> TemplateEnv
mkEnv = foldl f P.newEnv
  where f env (key, value) = P.addToEnv key value env

mkTests :: String -> TemplateEnv -> [(String, String)] -> TF.Test
mkTests prefix env templates = TF.testGroup prefix allTests
  where 
    allTests = zipWith (curry build) [(1::Int)..] templates
    build (i, (t, e)) = TFH.testCase (name i t e) $ do 
        assertString $ case P.parseExpr t of
            Left x  -> t ++ " => " ++ show x
            Right _ -> ""
        let n = name i t e
        let x = P.expand env t
        assertEqual n e x
        -- putStrLn ("\t" ++ t ++ " => " ++ x)
    name i t e = prefix ++ "." ++ show i ++ ": " ++ t ++ " => " ++ e

testOriginal :: TF.Test
testOriginal = mkTests "Original" env [(template, expected)]
  where 
    env = P.addListToEnv "vals" ["a", "b", "a cd e"] $ mkEnv [("searchTerm","google"),("type","int")]
    template = "http://google.com/q={searchTerm?}&{-join|&|type,searchTerm}"
    expected = "http://google.com/q=google&type=int&searchTerm=google"

envLevel1, envLevel2, envLevel3, envLevel4 :: TemplateEnv
testLevel1, testLevel2, testLevel3, testLevel4 :: TF.Test

envLevel1 = mkEnv [("var","value"),("hello","Hello World!")]
testLevel1 = mkTests "Level 1" envLevel1
    [ ("{var}", "value")
    , ("{hello}", "Hello%20World%21")
    ]

envLevel2 = mkEnv [("var","value"),("hello","Hello World!"),("path","/foo/bar")]
testLevel2 = mkTests "Level 2" envLevel2 
    [ ("{+var}", "value")
    , ("{+hello}", "Hello%20World!")
    , ("{+path}/here", "/foo/bar/here")
    , ("here?ref={+path}", "here?ref=/foo/bar")
    , ("X{#var}", "X#value")
    , ("X{#hello}", "X#Hello%20World!" )
    ]

envLevel3 = mkEnv 
    [ ("var","value")
    , ("hello","Hello World!")
    , ("path","/foo/bar")
    , ("empty","")
    , ("x","1024")
    , ("y","768")
    ]
testLevel3 = mkTests "Level 3" envLevel3
    [ ("map?{x,y}", "map?1024,768")
    , ("{x,hello,y}", "1024,Hello%20World%21,768")
    , ("{+x,hello,y}", "1024,Hello%20World!,768")
    , ("{+path,x}/here", "/foo/bar,1024/here")
    , ("{#x,hello,y}", "#1024,Hello%20World!,768")
    , ("{#path,x}/here", "#/foo/bar,1024/here")
    , ("X{.var}", "X.value")
    , ("X{.x,y}", "X.1024.768")
    , ("{/var}", "/value")
    , ("{/var,x}/here", "/value/1024/here")
    , ("{;x,y}", ";x=1024;y=768")
    , ("{;x,y,empty}", ";x=1024;y=768;empty")
    , ("{?x,y}", "?x=1024&y=768")
    , ("{?x,y,empty}", "?x=1024&y=768&empty=")
    , ("?fixed=yes{&x}", "?fixed=yes&x=1024")
    , ("{&x,y,empty}", "&x=1024&y=768&empty=")
    ]

envLevel4 = 
    P.addListToEnv "list" ["red", "green", "blue"] $
    P.addAssocToEnv "keys" [("semi",";"),("dot","."),("comma",",")] $ 
    mkEnv [("var","value"),("hello","Hello World!"),("path","/foo/bar")]
testLevel4 = mkTests "Level 4" envLevel4
    [ ("{var:3}", "val")
    , ("{var:30}", "value")
    , ("{list}", "red,green,blue")
    , ("{list*}", "red,green,blue")
    , ("{keys}", "semi,%3b,dot,.,comma,%2c")
    , ("{keys*}", "semi=%3b,dot=.,comma=%2c")
    , ("{+path:6}/here", "/foo/b/here")
    , ("{+list}", "red,green,blue")
    , ("{+list*}", "red,green,blue")
    , ("{+keys}", "semi,;,dot,.,comma,,")
    , ("{+keys*}", "semi=;,dot=.,comma=,")
    , ("{#path:6}/here", "#/foo/b/here")
    , ("{#list}", "#red,green,blue")
    , ("{#list*}", "#red,green,blue")
    , ("{#keys}", "#semi,;,dot,.,comma,,")
    , ("{#keys*}", "#semi=;,dot=.,comma=,")
    , ("X{.var:3}", "X.val")
    , ("X{.list}", "X.red,green,blue")
    , ("X{.list*}", "X.red.green.blue")
    , ("X{.keys}", "X.semi,%3b,dot,.,comma,%2c")
    , ("X{.keys*}", "X.semi=%3b.dot=..comma=%2c")
    , ("{/var:1,var}", "/v/value")
    , ("{/list}", "/red,green,blue")
    , ("{/list*}", "/red/green/blue")
    , ("{/list*,path:4}", "/red/green/blue/%2ffoo")
    , ("{/keys}", "/semi,%3b,dot,.,comma,%2c")
    , ("{/keys*}", "/semi=%3b/dot=./comma=%2c")
    , ("{;hello:5}", ";hello=Hello")
    , ("{;list}", ";list=red,green,blue")
    , ("{;list*}", ";list=red;list=green;list=blue")
    , ("{;keys}", ";keys=semi,%3b,dot,.,comma,%2c")
    , ("{;keys*}", ";semi=%3b;dot=.;comma=%2c")
    , ("{?var:3}", "?var=val")
    , ("{?list}", "?list=red,green,blue")
    , ("{?list*}", "?list=red&list=green&list=blue")
    , ("{?keys}", "?keys=semi,%3b,dot,.,comma,%2c")
    , ("{?keys*}", "?semi=%3b&dot=.&comma=%2c")
    , ("{&var:3}", "&var=val")
    , ("{&list}", "&list=red,green,blue")
    , ("{&list*}", "&list=red&list=green&list=blue")
    , ("{&keys}", "&keys=semi,%3b,dot,.,comma,%2c")
    , ("{&keys*}", "&semi=%3b&dot=.&comma=%2c")
    ]

envSec2_4_1, envSec2_4_2 :: TemplateEnv
testSec2_4_1, testSec2_4_2 :: TF.Test

envSec2_4_1 = mkEnv [("var","value"),("semi",";")]
testSec2_4_1 = mkTests "Section 2.4.1" envSec2_4_1
    [ ("{var}","value")
    , ("{var:20}","value")
    , ("{var:3}","val")
    , ("{semi}","%3b")
    , ("{semi:2}","%3b")
    ]

envSec2_4_2 = 
    P.addAssocToEnv "address" [("city","Newport Beach"),("state","CA")] $ 
    P.addListToEnv "year" ["1965","2000","2012"] $
    P.addListToEnv "dom" ["example","com"] newEnv
testSec2_4_2 = mkTests "Section 2.4.2" envSec2_4_2
    [ ("/mapper{?address*}", "/mapper?city=Newport%20Beach&state=CA")
    , ("find{?year*}", "find?year=1965&year=2000&year=2012")
    , ("www{.dom*}", "www.example.com")
    ]

envSec3_2 :: TemplateEnv
envSec3_2 = 
    P.addListToEnv "count" ["one", "two", "three"] $
    P.addListToEnv "dom" ["example","com"] $
    P.addListToEnv "list" ["red", "green", "blue"] $
    P.addListToEnv "empty_keys" [] $
    P.addNullToEnv "undef" $
    P.addAssocToEnv "keys" [("semi",";"),("dot","."),("comma",",")] $ 
    mkEnv 
        [ ("dub","me/too")
        , ("hello","Hello World!")
        , ("half","50%")
        , ("var","value")
        , ("who","fred")
        , ("base","http://example.com/home/")
        , ("path","/foo/bar")
        , ("v","6")
        , ("x","1024")
        , ("y","768")
        , ("empty","")
        , ("who","fred")
        ]

testSec3_2_1, testSec3_2_2, testSec3_2_3 :: TF.Test
testSec3_2_4, testSec3_2_5, testSec3_2_6 :: TF.Test
testSec3_2_7, testSec3_2_8, testSec3_2_9 :: TF.Test

testSec3_2_1 = mkTests "Section 3.2.1" envSec3_2
    [ ("{count}" , "one,two,three")
    , ("{count*}" , "one,two,three")
    , ("{/count}" , "/one,two,three")
    , ("{/count*}" , "/one/two/three")
    , ("{;count}" , ";count=one,two,three")
    , ("{;count*}" , ";count=one;count=two;count=three")
    , ("{?count}" , "?count=one,two,three")
    , ("{?count*}" , "?count=one&count=two&count=three")
    , ("{&count*}" , "&count=one&count=two&count=three")
    ]

testSec3_2_2 = mkTests "Section 3.2.2" envSec3_2
    [ ("{var}", "value")
    , ("{hello}", "Hello%20World%21")
    , ("{half}", "50%25")
    , ("O{empty}X", "OX")
    , ("O{undef}X", "OX")
    , ("{x,y}", "1024,768")
    , ("{x,hello,y}", "1024,Hello%20World%21,768")
    , ("?{x,empty}", "?1024,")
    , ("?{x,undef}", "?1024")
    , ("?{undef,y}", "?768")
    , ("{var:3}", "val")
    , ("{var:30}", "value")
    , ("{list}", "red,green,blue")
    , ("{list*}", "red,green,blue")
    , ("{keys}", "semi,%3b,dot,.,comma,%2c")
    , ("{keys*}", "semi=%3b,dot=.,comma=%2c")
    ]

testSec3_2_3 = mkTests "Section 3.2.3" envSec3_2
    [ ("{+var}",                "value")
    , ("{+hello}",              "Hello%20World!")
    , ("{+half}",               "50%25")

    , ("{base}index",           "http%3a%2f%2fexample.com%2fhome%2findex")
    , ("{+base}index",          "http://example.com/home/index")
    , ("O{+empty}X",            "OX")
    , ("O{+undef}X",            "OX")

    , ("{+path}/here",          "/foo/bar/here")
    , ("here?ref={+path}",      "here?ref=/foo/bar")
    , ("up{+path}{var}/here",   "up/foo/barvalue/here")
    , ("{+x,hello,y}",          "1024,Hello%20World!,768")
    , ("{+path,x}/here",        "/foo/bar,1024/here")

    , ("{+path:6}/here",        "/foo/b/here")
    , ("{+list}",               "red,green,blue")
    , ("{+list*}",              "red,green,blue")
    , ("{+keys}",               "semi,;,dot,.,comma,,")
    , ("{+keys*}",              "semi=;,dot=.,comma=,")
    ]

testSec3_2_4 = mkTests "Section 3.2.4" envSec3_2
    [ ("{#var}",             "#value")
    , ("{#hello}",           "#Hello%20World!")
    , ("{#half}",            "#50%25")
    , ("foo{#empty}",        "foo#")
    , ("foo{#undef}",        "foo")
    , ("{#x,hello,y}",       "#1024,Hello%20World!,768")
    , ("{#path,x}/here",     "#/foo/bar,1024/here")
    , ("{#path:6}/here",     "#/foo/b/here")
    , ("{#list}",            "#red,green,blue")
    , ("{#list*}",           "#red,green,blue")
    , ("{#keys}",            "#semi,;,dot,.,comma,,")
    , ("{#keys*}",           "#semi=;,dot=.,comma=,")
    ]

testSec3_2_5 = mkTests "Section 3.2.5" envSec3_2
    [ ("{.who}",             ".fred")
    , ("{.who,who}",         ".fred.fred")
    , ("{.half,who}",        ".50%25.fred")
    , ("www{.dom*}",         "www.example.com")
    , ("X{.var}",            "X.value")
    , ("X{.empty}",          "X.")
    , ("X{.undef}",          "X")
    , ("X{.var:3}",          "X.val")
    , ("X{.list}",           "X.red,green,blue")
    , ("X{.list*}",          "X.red.green.blue")
    , ("X{.keys}",           "X.semi,%3b,dot,.,comma,%2c")
    , ("X{.keys*}",          "X.semi=%3b.dot=..comma=%2c")
    , ("X{.empty_keys}",     "X")
    , ("X{.empty_keys*}",    "X")
    ]

testSec3_2_6 = mkTests "Section 3.2.6" envSec3_2
    [ ("{/who}",             "/fred")
    , ("{/who,who}",         "/fred/fred")
    , ("{/half,who}",        "/50%25/fred")
    , ("{/who,dub}",         "/fred/me%2ftoo")
    , ("{/var}",             "/value")
    , ("{/var,empty}",       "/value/")
    , ("{/var,undef}",       "/value")
    , ("{/var,x}/here",      "/value/1024/here")
    , ("{/var:1,var}",       "/v/value")
    , ("{/list}",            "/red,green,blue")
    , ("{/list*}",           "/red/green/blue")
    , ("{/list*,path:4}",    "/red/green/blue/%2ffoo")
    , ("{/keys}",            "/semi,%3b,dot,.,comma,%2c")
    , ("{/keys*}",           "/semi=%3b/dot=./comma=%2c")
    ]

testSec3_2_7 = mkTests "Section 3.2.7" envSec3_2
    [ ("{;who}",             ";who=fred")
    , ("{;half}",            ";half=50%25")
    , ("{;empty}",           ";empty")
    , ("{;v,empty,who}",     ";v=6;empty;who=fred")
    , ("{;v,bar,who}",       ";v=6;who=fred")
    , ("{;x,y}",             ";x=1024;y=768")
    , ("{;x,y,empty}",       ";x=1024;y=768;empty")
    , ("{;x,y,undef}",       ";x=1024;y=768")
    , ("{;hello:5}",         ";hello=Hello")
    , ("{;list}",            ";list=red,green,blue")
    , ("{;list*}",           ";list=red;list=green;list=blue")
    , ("{;keys}",            ";keys=semi,%3b,dot,.,comma,%2c")
    , ("{;keys*}",           ";semi=%3b;dot=.;comma=%2c")
    ]

testSec3_2_8 = mkTests "Section 3.2.8" envSec3_2
    [ ("{?who}",             "?who=fred")
    , ("{?half}",            "?half=50%25")
    , ("{?x,y}",             "?x=1024&y=768")
    , ("{?x,y,empty}",       "?x=1024&y=768&empty=")
    , ("{?x,y,undef}",       "?x=1024&y=768")
    , ("{?var:3}",           "?var=val")
    , ("{?list}",            "?list=red,green,blue")
    , ("{?list*}",           "?list=red&list=green&list=blue")
    , ("{?keys}",            "?keys=semi,%3b,dot,.,comma,%2c")
    , ("{?keys*}",           "?semi=%3b&dot=.&comma=%2c")
    ]

testSec3_2_9 = mkTests "Section 3.2.9" envSec3_2
    [ ("{&who}",             "&who=fred")
    , ("{&half}",            "&half=50%25")
    , ("?fixed=yes{&x}",     "?fixed=yes&x=1024")
    , ("{&x,y,empty}",       "&x=1024&y=768&empty=")
    , ("{&x,y,undef}",       "&x=1024&y=768")

    , ("{&var:3}",           "&var=val")
    , ("{&list}",            "&list=red,green,blue")
    , ("{&list*}",           "&list=red&list=green&list=blue")
    , ("{&keys}",            "&keys=semi,%3b,dot,.,comma,%2c")
    , ("{&keys*}",           "&semi=%3b&dot=.&comma=%2c")
    ]

main :: IO () 
main = TF.defaultMain $
    [ testLevel1
    , testLevel2
    , testLevel3
    , testLevel4
    , testSec2_4_1
    , testSec2_4_2
    , testSec3_2_1
    , testSec3_2_2
    , testSec3_2_3
    , testSec3_2_4
    , testSec3_2_5
    , testSec3_2_6
    , testSec3_2_7
    , testSec3_2_8
    , testSec3_2_9
    ]