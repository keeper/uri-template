module Main where
import Debug.Trace(trace)
import Network.URI.Template as P
import qualified Data.Map as M
import Test.HUnit

exprs = good ++ naughty

good =
    [ "{+asdf:20}"
    , "{+asdf:}"
    , "{+asdf%20}"
    , "O{empty}X"
    , "http://www.google.com/{+asdf%20}"
    , "http://www.google.com/{asdf}/{fdsa}"
    , "http://www.google.com/{+asdf:}"
    ]

naughty = 
    [ "{+asdf%2}"
    , "http://www.google.com/{+asdf"
    , "http://www.google.com/{+asdf%2"
    , "http://www.google.com/{+asdf%2}"
    ]

testExpr :: String -> String
testExpr input = case P.parseExpr input of
    Left err -> "* No match: " ++ input ++ "\n" ++ show err ++ "\n"
    Right val -> "* Found value: " ++ show val ++ "\n"

main = mapM_ (putStrLn . testExpr) exprs >> putStrLn "* done."