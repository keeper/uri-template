{-
 Codec for de/encoding URI strings via percent encodings
 (cf. RFC 3986.)
-}
module Codec.Web.Percent 
  ( getEncodedString -- :: String -> Bool -> String
  , getDecodedString -- :: String -> String
  , getEncodedChar   -- :: Char -> Bool -> Maybe String
  , getDecodedChar   -- :: String -> Maybe (Char, String)
  )
where

import Data.Char ( chr, isAlphaNum )
import Numeric   ( readHex, showHex )

genDelims :: String
genDelims = ":/?#[]@"

subDelims :: String
subDelims = "!$&'()*+,;="

reserved :: Char -> Bool
reserved c = c `elem` genDelims ++ subDelims

unreserved :: Char -> Bool
unreserved c = isAlphaNum c || c `elem` "-._~"

getEncodedString :: String -> Bool -> String
getEncodedString "" _ = ""
getEncodedString (x:xs) res = 
  case getEncodedChar x res of
    Nothing -> x : getEncodedString xs res
    Just ss -> ss ++ getEncodedString xs res

getDecodedString :: String -> String
getDecodedString "" = ""
getDecodedString ls@(x:xs) = 
  case getDecodedChar ls of
    Nothing -> x : getDecodedString xs
    Just (ch,xs1) -> ch : getDecodedString xs1

getEncodedChar :: Char -> Bool -> Maybe String
getEncodedChar x res
 | unreserved x      = Nothing
 | res && reserved x = Nothing
 | xi < 0xff         = Just ('%':showHex (xi `div` 16) (showHex (xi `mod` 16) ""))
 | otherwise         = -- ToDo: import utf8 lib
   error "getEncodedChar: can only handle 8-bit chars right now."
 where
  xi :: Int
  xi = fromEnum x

getDecodedChar :: String -> Maybe (Char, String)
getDecodedChar str =
 case str of
   ""          -> Nothing
   (x:xs) 
    | x /= '%'  -> Nothing
    | otherwise -> do
       case xs of
         (b1:b2:bs) -> 
	    case readHex [b1,b2] of
	      ((v,_):_) -> Just (Data.Char.chr v, bs)
	      _ -> Nothing
	 _ -> Nothing
