module Network.URI.Template
    ( TemplateEnv(..)
    , EnvItem(..)
    , newEnv        -- :: TemplateEnv
    , addToEnv      -- :: TemplateEnv -> String -> String -> TemplateEnv
    , addListToEnv  -- :: TemplateEnv -> String -> [String] -> TemplateEnv
    , addAssocToEnv -- :: TemplateEnv -> String -> [(String, String)] -> TemplateEnv
    , addNullToEnv  -- :: TemplateEnv -> String -> TemplateEnv
    , parseExpr     -- :: String -> Either ParseError Template
    , expand        -- :: TemplateEnv -> String -> String
    )
where
import Data.Map (Map)
import qualified Data.Map as M
import Data.List (intercalate)
import Data.Maybe (mapMaybe)
import Text.ParserCombinators.Parsec
import Codec.Binary.UTF8.String as UTF8
import Codec.Web.Percent

--| @Var@ represents the individual variable references in the template
data Var = Var String (Maybe Modifier) deriving (Show)

--| @Modifier@ defines the allowed template modifiers
data Modifier
        = ModPrefix Int
        | ModExplode
    deriving (Show)

--| @Oper@ defines the allowed template operators
data Oper
        = OpNone
        | OpReserved
        | OpFragment
        | OpDot
        | OpSemi
        | OpSlash
        | OpQuery
        | OpQueryCont
    deriving (Show)

--| @OperRule@ defines the rules for the various template operators
data OperRule = OperRule
        { prefix :: String
        , sep    :: String
        , addKey :: Bool
        , addEq  :: Bool
        , useRes :: Bool
        } deriving Show

--| @TemplateItem@ defines the types for parsing a template
data TemplateItem
        = Quote String
        | Expr Oper [Var]
    deriving (Show)
--| @Template@ is a list of @TemplateItem@
type Template = [TemplateItem]

--| @TemplateEnv@ holds the key/value mapping for the expansion
--  context for a URI template.
newtype TemplateEnv = TemplateEnv { tenv :: Map String EnvItem } deriving (Show)

--| @EnvItem@ defines the value types allowed for the URI 
--  template expansion context
data EnvItem
        = TString String
        | TList   [String]
        | TAssoc  [(String, String)]
        | TNull
    deriving (Show)

--| get the rules for an operator
rules :: Oper -> OperRule
rules OpReserved  = OperRule ""  "," False False True
rules OpFragment  = OperRule "#" "," False False True
rules OpDot       = OperRule "." "." False False False
rules OpSlash     = OperRule "/" "/" False False False
rules OpSemi      = OperRule ";" ";" True  False False
rules OpQuery     = OperRule "?" "&" True  True  False
rules OpQueryCont = OperRule "&" "&" True  True  False
rules OpNone      = OperRule ""  "," False False False

--| Construct a new, empty 'TemplateEnv'.
newEnv :: TemplateEnv
newEnv = TemplateEnv{tenv=M.empty}

--| UTF-8ifies the RHS, followed by percent encoding it.
normalize :: Bool -> String -> String
normalize res s = getEncodedString (UTF8.encodeString s) res

--| @addToEnv key value env@ augments @env@ with a new
--  key,value pair.
addToEnv :: String -> String -> TemplateEnv -> TemplateEnv
addToEnv k v env = TemplateEnv{tenv=M.insert k (TString v) (tenv env)}

--| @addListToEnv key vals env@ expands the template environment @env@
--  with a list-valued key,value(s) pair.
addListToEnv :: String -> [String] -> TemplateEnv -> TemplateEnv
addListToEnv k vs env =
    if (length vs) > 0
        then TemplateEnv{tenv=M.insert k (TList vs) (tenv env)}
        else addNullToEnv k env

--| @addAssocToEnv key vals env@ expands the template environment @env@
--  with a associative array key,value(s) pair.
addAssocToEnv :: String -> [(String, String)] -> TemplateEnv -> TemplateEnv
addAssocToEnv k vs env =
    if (length vs) > 0
        then TemplateEnv{tenv=M.insert k (TAssoc vs) (tenv env)}
        else addNullToEnv k env

--| @addNullToEnv key vals env@ expands the template environment @env@
--  with a key, null value pair.
addNullToEnv :: String -> TemplateEnv -> TemplateEnv
addNullToEnv k env
 = TemplateEnv{tenv=M.insert k TNull (tenv env)}

--| operator parser
operator :: Parser Oper
operator = do
    x <- oneOf ("+#./;?&" ++ "=,!@|")
    return (case x of
        '+' -> OpReserved
        '#' -> OpFragment
        '.' -> OpDot
        '/' -> OpSlash
        ';' -> OpSemi
        '?' -> OpQuery
        '&' -> OpQueryCont
        _   -> OpNone)

--| variable list parser
varList :: Parser [Var]
varList = varName `sepBy` char ','
    where
        varName :: Parser Var
        varName = do
            x <- varText
            m <- optionMaybe modifier
            return (Var x m)

        varText :: Parser String
        varText = do
                x <- many1 (pctEncoded <|> many1 (alphaNum <|> char '_'))
                return $ concat x

        pctEncoded :: Parser String
        pctEncoded = do
            x <- char '%'
            y <- count 2 hexDigit
            return $ concat [[x]] ++ y

        modifier :: Parser Modifier
        modifier = modPrefix <|> do { _ <- char '*'; return ModExplode }

        modPrefix :: Parser Modifier
        modPrefix = do
            _ <- char ':'
            dig <- many digit
            let n = case reads dig of
                     []       -> 0 :: Int
                     (x, _):_ -> x
            return (ModPrefix n)

--| template parser
parseTemplate :: Parser Template
parseTemplate = many1 $ (try quoteEof) <|> (try expr) <|> quotedValue
    where
        quoteEof :: Parser TemplateItem
        quoteEof = do
            f <- many1 (noneOf "{}")
            return (Quote f)
        expr :: Parser TemplateItem
        expr = between (char '{') (char '}') $ do
            o <- option OpNone operator
            x <- varList
            return (Expr o x)
        quotedValue :: Parser TemplateItem
        quotedValue = do
            f <- manyTill anyChar (lookAhead expr)
            return (Quote f)

--| attempt to parse the template
parseExpr :: String -> Either ParseError Template
parseExpr input = parse parseTemplate "uri-template" input

--| expand string variables
expandTString :: OperRule -> (String, String) -> String
expandTString rule (k, v) = concat [n, e, v]
    where
        n = if addKey rule then k else ""
        e = if addEq rule || (addKey rule && not (null v)) then "=" else ""

--| expand list variables
expandTList :: OperRule -> Bool -> (String, [String]) -> String
expandTList rule explode (k, vs) =
    if explode
        then intercalate (sep rule) $ map (\v -> concat [n, e v, v]) vs
        else concat [n, e vNormal, vNormal]
    where
        n = if addKey rule then k else ""
        e v = if addEq rule || (addKey rule && not (null v)) then "=" else ""
        vNormal = intercalate "," vs

--| expand assoc array variables
expandTAssoc :: OperRule -> Bool -> String -> [(String, String)] -> String
expandTAssoc rule explode ak items = n ++ (intercalate s $ map item items)
    where
        n = if not explode && addKey rule then ak ++ "=" else ""
        c = if explode then "=" else ","
        s = if explode then (sep rule) else ","
        item (k, v) = k ++ c ++ v

--| expand individual variables
expandVar :: OperRule -> TemplateEnv -> Var -> Maybe String
expandVar rule env (Var k md) =
        case M.lookup k (tenv env) of
            Nothing          -> Nothing
            Just TNull       -> Nothing
            Just (TString x) -> Just $ expandTString rule (k, nt x)
            Just (TList x)   -> Just $ expandTList rule explode (k, map nt x)
            Just (TAssoc x)  -> Just $ expandTAssoc rule explode k $ map (\(nk, i) -> (nk, nt i)) x
    where
        maxChars = case md of
            Just (ModPrefix n) -> n
            _                  -> 0
        explode = case md of
            Just ModExplode -> True
            _               -> False
        nt = norm.trunc
        trunc x = if maxChars > 0 then take maxChars x else x
        norm x = normalize (useRes rule) x

--| expand an individual @TemplateItem@
expandItem :: TemplateEnv -> TemplateItem -> String
expandItem _   (Quote x)      = x
expandItem env (Expr op vars) = p ++ i
    where
        p = if (length m) > 0 then prefix r else ""
        r = rules op
        i = intercalate (sep r) m
        m = mapMaybe (expandVar r env) vars

--| expand a URI template
expand :: TemplateEnv -> String -> String
expand env input = case parseExpr input of
    Left _  -> input
    Right t -> concatMap (expandItem env) t
